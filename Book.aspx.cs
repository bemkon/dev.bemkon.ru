﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfessorTesting
{
    public partial class BookPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Научные работы и статьи. " + Core.Site.titleProgram;
        }

        protected void Books_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = (DataRowView)e.Item.DataItem;

                Image img = (Image)e.Item.FindControl("BookImage");
                img.ImageUrl = string.Format("{0}Books/{1}", css_MasterPage.DBImageRoot(), row["BookImageFile"].ToString());
                bool forSale = Convert.ToBoolean(row["ForSale"]);
                Panel p = (Panel)e.Item.FindControl("PricePanel");
                p.Visible = forSale;
                p = (Panel)e.Item.FindControl("PublisherPanel");
                p.Visible = row["Publisher"].ToString() != "";
                p = (Panel)e.Item.FindControl("FormatPanel");
                p.Visible = row["Format"].ToString() != "";
                p = (Panel)e.Item.FindControl("ISBNPanel");
                p.Visible = row["ISBN"].ToString() != "";
                p = (Panel)e.Item.FindControl("PagesPanel");
                p.Visible = !row["Pages"].Equals(DBNull.Value);
                Literal l = (Literal)e.Item.FindControl("ElectronicBook");
                l.Visible = Convert.ToBoolean(row["Binary"]);
                l = (Literal)e.Item.FindControl("Comment");
                string cm = row["Comment"].ToString();
                if (cm != "")
                {
                    l.Text = "*" + cm;
                }
            }
        }
    }
}