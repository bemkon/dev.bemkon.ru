﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeBehind="Book.aspx.cs" Inherits="ProfessorTesting.BookPage" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main">
        <div style="margin-left: 30px; margin-right: 30px">
            <center><h2>Дорогие гости!</h2></center>
            <center>Вашему вниманию предлагают последние работы основателей системы Бемкон.У нас Вы можете 
                Заказать их без наценок книжных магазинов, <font color="red">а с некоторыми ознакомиться совершенно бесплатно!</font></center><br />
            <center><h2>Dear guests!</h2></center>
            <center>We offer last works of founders of the unique system Bemkon. 
                Here you can order them with no extra charge  <font color="red">and some works are available absolutely free!</font></center><br />
            <asp:Repeater runat="server" ID="Books" DataSourceID="BooksDS" OnItemDataBound="Books_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0"><tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="td_img"><asp:Image runat="server" ID="BookImage" CssClass="book_img" /></td>
                        <td class="td_text">
                            <span class="book_name"><asp:Literal runat="server" ID="BookName" Text='<%# Eval("BookName") %>'></asp:Literal>.</span>
                            <span class="book_authors"><asp:Literal runat="server" ID="BookAuthors" Text='<%# Eval("Authors") %>'></asp:Literal>.</span>
                            <div class="book_description"><asp:Literal runat="server" ID="Description" Text='<%# Eval("Description") %>'></asp:Literal></div>
                            <asp:Panel runat="server" ID="PricePanel">
                                <asp:Literal runat="server" ID="PricePrompt" Text='<%# Eval("PricePrompt") %>'>
                                </asp:Literal>:&nbsp;<span class="book_price"><asp:Literal runat="server" ID="Price" Text='<%# Eval("Price") %>'>
                                    </asp:Literal>&nbsp;<asp:Literal runat="server" ID="Currency" Text='<%# Eval("Currency") %>'></asp:Literal></span>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="PublisherPanel">
                                <asp:Literal runat="server" ID="PublisherPrompt" Text='<%# Eval("PublisherPrompt") %>'>
                                </asp:Literal>:&nbsp;<asp:Literal runat="server" ID="Publisher" Text='<%# Eval("Publisher") %>'>
                                </asp:Literal>,&nbsp;<asp:Literal runat="server" ID="Year" Text='<%# Eval("Year") %>'>
                                </asp:Literal>&nbsp;<asp:Literal runat="server" ID="YearPrompt" Text='<%# Eval("YearPrompt") %>'></asp:Literal>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="FormatPanel">
                                <asp:Literal runat="server" ID="FormatPrompt" Text='<%# Eval("FormatPrompt") %>'>
                                </asp:Literal>:&nbsp;<asp:Literal runat="server" ID="Format" Text='<%# Eval("Format") %>'></asp:Literal>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="ISBNPanel">
                                ISBN:&nbsp;<asp:Literal runat="server" ID="ISBN" Text='<%# Eval("ISBN") %>'></asp:Literal>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="PagesPanel">
                                <asp:Literal runat="server" ID="Pages" Text='<%# Eval("Pages") %>'>
                                </asp:Literal>&nbsp;<asp:Literal runat="server" ID="PagePrompt" Text='<%# Eval("PagePrompt") %>'></asp:Literal>
                            </asp:Panel>
                            <div class="book_electronic"><asp:Literal runat="server" ID="ElectronicBook" Text="Книга отправляется в электронном формате."></asp:Literal></div>
                            <div class="book_comment"><asp:Literal runat="server" ID="Comment"></asp:Literal></div>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:SqlDataSource runat="server" ID="BooksDS" ConnectionString='<%$ ConnectionStrings: MyConnectionString %>'
                SelectCommand="select * from dbo.Books order by SortOrder, BookName"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
