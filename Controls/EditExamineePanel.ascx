﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditExamineePanel.ascx.cs" Inherits="ProfessorTesting.Controls.EditExamineePanel" %>

<div style="padding-bottom:20px;">
<asp:Label ID="LabelTitle" runat="server" 
Text="Редактирование обследуемого" CssClass="header_block" >
</asp:Label>
</div>

<div>
    <table style="width: 464px" border="0" cellpadding="5">
        <tr>
            <td colspan="3" style="color:Red;">
                <asp:Label runat="server" ID="labelError"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="labelID">Номер:</asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="textBoxID" ReadOnly="True" Width="79px"></asp:TextBox>
            </td>
            <td rowspan="2" align="right" valign="top">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/User_48x48.png" />
            </td>
        </tr>
        
        <tr>
            <td>
                <asp:Label runat="server" ID="labelName">Имя:</asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="textBoxName" Width="263px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldName" runat="server" 
                    ControlToValidate="textBoxName" ErrorMessage="Введите имя обследуемого">*</asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label runat="server" ID="labelTests">Тесты:</asp:Label>
            </td>
            <td colspan="2">
                <asp:Repeater runat="server" ID="listViewTests" >
                </asp:Repeater>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label runat="server" ID="labelComments">Комментарии:</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="textBoxComments" Rows="3" TextMode="MultiLine" 
                    Width="343px"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td colspan="3">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
            </td>
        </tr>

        <tr>
            <td colspan="3" align="center" style="padding-top:20px;">
                <asp:LinkButton runat="server" ID="buttonEditSave">Сохранить</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;
                <asp:HyperLink runat="server" ID="buttonClose" NavigateUrl="~/User/">Отмена</asp:HyperLink>
            </td>
        </tr>
    </table>
</div>

